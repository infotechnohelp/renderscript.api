<?php declare(strict_types=1);

namespace RenderScript\Api\Lib;

use RenderScript\Lib\Component;

class ApiComponent extends Component{

    public function __construct(string $__CLASS__, string $componentTitle = null)
    {
        parent::__construct($__CLASS__, $componentTitle, true);
    }
}