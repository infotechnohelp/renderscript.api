<?php declare(strict_types=1);

namespace RenderScript\Api\Lib;

use RenderScript\Lib\Recipe;
use RenderScript\Lib\TwigRenderer\RenderingSettings;

class ApiRecipe extends Recipe
{
    public function __construct(RenderingSettings $settings = null)
    {
        parent::__construct(
            $settings, 
            dirname(__DIR__, 5). '/', 
            true
        );
    }
}