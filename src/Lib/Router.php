<?php declare(strict_types=1);

namespace RenderScript\Api\Lib;

use RenderScript\Api\Controller\MainController;

class Router
{
    public function resolveRoute(){

        switch ($_SERVER['REQUEST_URI']) {
            case '/':
                (new MainController())->default();
                break;
            case '/token':
                (new MainController())->token();
                break;
        }
    }
}