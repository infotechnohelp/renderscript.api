<?php

use OAuth2\Autoloader;

Autoloader::register();

$config = require_once dirname(__DIR__, 3) . '/config/db.php';

$storage = new OAuth2\Storage\Pdo([
    'dsn' => "mysql:host={$config['host']};dbname={$config['dbname']}",
    'username' => $config['username'],
    'password' => $config['password']
]);
$server = new OAuth2\Server($storage);

$server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

return $server;